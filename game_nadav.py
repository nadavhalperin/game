__author__ = 'Cyber-01'

import pygame
import math
from random import shuffle
import random
from Tkinter import *
import functools

COLOR_PLAYER = {"red": (255, 0, 0), "green": (0, 153, 51), "blue": (0, 0, 255)}
COLOR_LIST = ["red", "green", "blue"]


class Player:

    def __init__(self, color, name):
        self.color = color
        self.dev_cards = []
        self.victory_points = 2
        self.resource_wheat = 0
        self.resource_iron = 0
        self.resource_brick = 0
        self.resource_sheep = 0
        self.resource_wood = 0
        self.development_cards = []
        self.used_knights = 0
        self.available_cities = 4
        self.available_settlements = 3
        self.available_roads = 15
        self.name = name
        self.color_rgb = COLOR_PLAYER[self.color]
        self.bank_trades = ["4:1"]

    def winner(self):
        print self.name, "won, he got his 10 victory points with:", 5 - self.available_settlements, "settlement/s,",\
            4 - self.available_cities, "city/ies, and ",\
            10 - (5 - self.available_settlements) - 2*(4 - self.available_cities), "wild victory point/s"

    def add_resource(self, resource, amount):
        for i in xrange(amount):
            if resource == "wheat":
                self.resource_wheat += 1
            if resource == "brick":
                self.resource_brick += 1
            if resource == "iron":
                self.resource_iron += 1
            if resource == "wood":
                self.resource_wood += 1
            if resource == "sheep":
                self.resource_sheep += 1

    def bought_road(self, first):
        if first:
            return
        self.resource_wood -= 1
        self.resource_brick -= 1
        self.available_roads -= 1

    def bought_sett(self):
        self.resource_wood -= 1
        self.resource_brick -= 1
        self.resource_sheep -= 1
        self.resource_wheat -= 1
        self.available_settlements -= 1
        self.victory_points += 1

    def bought_city(self):
        self.resource_wheat -= 2
        self.resource_iron -= 3
        self.available_cities -= 1
        self.available_settlements += 1
        self.victory_points += 1

    def bought_dev(self):
        self.resource_sheep -= 1
        self.resource_iron -= 1
        self.resource_wheat -= 1

    def trade(self, amount, give, get):
        if give == "wheat":
            self.resource_wheat -= amount
        elif give == "brick":
            self.resource_brick -= amount
        elif give == "iron":
            self.resource_iron -= amount
        elif give == "wood":
            self.resource_wood -= amount
        elif give == "sheep":
            self.resource_sheep -= amount
        if get == "wheat":
            self.resource_wheat += 1
        elif get == "brick":
            self.resource_brick += 1
        elif get == "iron":
            self.resource_iron += 1
        elif get == "wood":
            self.resource_wood += 1
        elif get == "sheep":
            self.resource_sheep += 1

    def what_resource(self, resource):
        if resource == "wheat":
            return self.resource_wheat
        if resource == "brick":
            return self.resource_brick
        if resource == "iron":
            return self.resource_iron
        if resource == "wood":
            return self.resource_wood
        if resource == "sheep":
            return self.resource_sheep


class Road:
    def __init__(self, crossroad_1, crossroad_2, area):
        self.occupied = False
        self.crossroad_1 = crossroad_1
        self.crossroad_2 = crossroad_2
        self.area = area

    def occupied_by_player(self, player):
        self.occupied = True
        self.crossroad_1.roads.append(player.name)
        self.crossroad_2.roads.append(player.name)

    def is_connected(self, player):
        if player.name in (self.crossroad_1.roads or self.crossroad_2.roads) \
                or self.crossroad_1.occupied_by == player.name \
                or self.crossroad_2.occupied_by == player.name:
            return True
        else:
            return False


class Crossroad:
    def __init__(self, location):
        self.location = location
        self.settled = False
        self.location_x = int(location[0])
        self.location_y = int(location[1])
        self.near_crossroads = []
        self.can_be_settled = True
        self.roads = []
        self.occupied_by = None
        self.player_occuping = None
        self.city = False
        self.special_port = None

    def near_crossroad(self, crossroad):
        if 64 < math.sqrt((self.location[0] - crossroad.location[0])**2 + (self.location[1] - crossroad.location[1])**2) < 66:
            self.near_crossroads.append(crossroad)

    def build_settlement(self, window, first, player):
        if self.can_be_settled and (player.name in self.roads or first):
            if self.special_port is not None:
                player.bank_trades.append(self.special_port)
                print player.bank_trades
            for nearby_crossroad in self.near_crossroads:
                nearby_crossroad.can_be_settled_change()
                self.can_be_settled = False
                self.settled = True
                pygame.draw.circle(window, player.color_rgb, (int(self.location[0]), int(self.location[1])), 15)
                self.occupied_by = player.name
                self.player_occuping = player
            return True
        else:
            print "cant build a settlement"
            return False

    def build_city(self, win, player):
        if self.settled and self.occupied_by == player.name:
            pygame.draw.circle(win, (0, 0, 0), (int(self.location[0]), int(self.location[1])), 10)
            self.city = True
            player.bought_city()
            return True
        else:
            print "cant build city"
            return False

    def can_be_settled_change(self):
        self.can_be_settled = False


class Hex:

    def __init__(self, location, resource, dice_num, color):
        self.location = location
        self.resource = resource
        self.dice_number = dice_num
        self.color = color
        self.crossroads = []
        self.robber_on = False
        self.crossroads_by = []
        self.pts = []
        self.hex_area = None
        if self.resource == "desert":
            self.robber_on = True
        x, y = self.location
        for i in range(6):
            x = x + 65 * math.cos(math.pi/6 + math.pi * 2 * i / 6)
            y = y + 65 * math.sin(math.pi/6 + math.pi * 2 * i / 6)
            self.crossroads.append([int(x), int(y)])

    def print_crossroad(self):
        print self.crossroads

    def calc_pts(self):
        x, y = self.location
        for i in range(6):
            x = x + 65 * math.cos(math.pi/6 + math.pi * 2 * i / 6)
            y = y + 65 * math.sin(math.pi/6 + math.pi * 2 * i / 6)
            self.pts.append([x, y])
            if [int(x), int(y)] not in self.crossroads:
                self.crossroads.append([int(x), int(y)])
        return self.pts

    def draw_hex(self, surface):
        x, y = self.location
        for i in range(6):
            x = x + 65 * math.cos(math.pi/6 + math.pi * 2 * i / 6)
            y = y + 65 * math.sin(math.pi/6 + math.pi * 2 * i / 6)
            self.pts.append([x, y])

        self.hex_area = pygame.draw.polygon(surface, self.color, self.pts)

        for i in range(6):
            if i == 5:
                pygame.draw.line(surface, (0, 0, 0), self.pts[i], self.pts[0])
            else:
                pygame.draw.line(surface, (0, 0, 0), self.pts[i], self.pts[i+1])
        center_x, center_y = self.location
        center_y -= 2*(65 * math.sin(math.pi/6 + math.pi * 2 * 3 / 6))
        if self.resource != "desert":
            pygame.font.init()
            myfont = pygame.font.SysFont('Comic Sans MS', 26)
            pygame.draw.circle(surface, (255, 255, 255), (int(center_x), int(center_y)), 22)
            label = myfont.render(str(self.dice_number), 1, (0, 0, 0))
            surface.blit(label, (int(center_x) - (14 if self.dice_number > 9 else 8), int(center_y) - 17))
        else:
            pygame.draw.circle(surface, (0, 0, 0), (int(center_x), int(center_y)), 22)

    def get_next_y(self):
        return int(self.crossroads[3][1])

    def robber(self, surface):
        self.robber_on = not self.robber_on
        center_x, center_y = self.location
        center_y -= 2*(65 * math.sin(math.pi/6 + math.pi * 2 * 3 / 6))
        if self.robber_on:
            pygame.draw.circle(surface, (0, 0, 0), (int(center_x), int(center_y)), 22)
        else:
            if self.resource == "desert":
                pygame.draw.circle(surface, self.color, (int(center_x), int(center_y)), 22)
            else:
                pygame.font.init()
                myfont = pygame.font.SysFont('Comic Sans MS', 26)
                pygame.draw.circle(surface, (255, 255, 255), (int(center_x), int(center_y)), 22)
                label = myfont.render(str(self.dice_number), 1, (0, 0, 0))
                surface.blit(label, (int(center_x) - (14 if self.dice_number > 9 else 8), int(center_y) - 17))

    def add_crossroad(self, replacement):
        self.crossroads_by.append(replacement)

    def got_rolled(self):
        for cross in self.crossroads_by:
            if cross.occupied_by is not None:
                print cross.player_occuping.name, "got ", self.resource
                if cross.city:
                    cross.player_occuping.add_resource(self.resource, 2)
                else:
                    cross.player_occuping.add_resource(self.resource, 1)


def draw_board(board_window):
    wheat_hex_color = (229, 219, 79)
    wood_hex_color = (75, 132, 90)
    sheep_hex_color = (11, 255, 77)
    iron_hex_color = (165, 168, 169)
    brick_hex_color = (104, 42, 24)
    desert_hex_color = (244, 113, 66)

    hex_lst = [("wheat", wheat_hex_color), ("wheat", wheat_hex_color), ("wheat", wheat_hex_color),
               ("wheat", wheat_hex_color),
               ("wood", wood_hex_color), ("wood", wood_hex_color), ("wood", wood_hex_color), ("wood", wood_hex_color),
               ("sheep", sheep_hex_color), ("sheep", sheep_hex_color), ("sheep", sheep_hex_color),
               ("sheep", sheep_hex_color),
               ("iron", iron_hex_color), ("iron", iron_hex_color), ("iron", iron_hex_color),
               ("brick", brick_hex_color), ("brick", brick_hex_color), ("brick", brick_hex_color),
               ("desert", desert_hex_color)]
    nums = [2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12]

    shuffle(hex_lst)
    shuffle(nums)

    hexagons = []

    num_place = 0
    y = 45
    x = 463
    hex_jump = 2*(65 * math.cos(math.pi/6 + math.pi * 2 * 5 / 6))
    row_x_1 = x - (hex_jump/2)
    row_x_2 = x - hex_jump

    for i in range(19):
        if hex_lst[i][0] != "desert":
            hexagons.append(Hex((x, y), hex_lst[i][0], nums[num_place], hex_lst[i][1]))
            num_place += 1
        else:
            hexagons.append(Hex((x, y), hex_lst[i][0], 0, hex_lst[i][1]))

        x += hex_jump
        if i == 2 or i == 6 or i == 11 or i == 15:
            if i == 2 or i == 11:
                x = row_x_1
            elif i == 6:
                x = row_x_2
            else:
                x = 463
            y = hexagons[i].get_next_y()

    for hexagon in hexagons:
        hexagon.draw_hex(board_window)

    crossroads, crossroads_area = create_crossroads(hexagons)
    road_areas = create_roads(crossroads)
    roads_area = draw_road_area(road_areas)
    roads = create_roads_list(roads_area)
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 16)

    dice_label = myfont.render("Roll dice", 1, (0, 0, 0))
    road_label = myfont.render("Build road", 1, (0, 0, 0))
    town_label = myfont.render("Build town", 1, (0, 0, 0))
    player_label = myfont.render("Next", 1, (0, 0, 0))
    cancel_label = myfont.render("Cancel", 1, (0, 0, 0))
    draw_card_label = myfont.render("dev card", 1, (0, 0, 0))
    city_label = myfont.render("build city", 1, (0, 0, 0))
    trade_label = myfont.render("trade", 1, (0, 0, 0))

    pygame.draw.rect(board_window, (0, 0, 0), (0, 0, 220, 600))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 20, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 55, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 90, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 125, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 560, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 160, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (20, 195, 90, 30))
    pygame.draw.rect(board_window, (255, 255, 255), (115, 55, 90, 30))

    board_window.blit(dice_label, (33, 24))
    board_window.blit(road_label, (26, 59))
    board_window.blit(town_label, (26, 94))
    board_window.blit(player_label, (45, 129))
    board_window.blit(cancel_label, (35, 564))
    board_window.blit(draw_card_label, (33, 164))
    board_window.blit(city_label, (32, 199))
    board_window.blit(trade_label, (137, 59))

    return hexagons, roads, crossroads, crossroads_area


def create_ports(crossroads, board_window):

    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 16)

    ports = [0, 9, 10, 13, 27, 28, 52, 53, 47, 51, 37, 45, 22, 23, 3, 17, 39, 40]

    label = ["3:1", "3:1", "3:1", "3:1", "2:1 wheat", "2:1 wood", "2:1 brick", "2:1 sheep", "2:1 iron"]

    for i in xrange(len(ports)):
        port = ports[i]
        cross = crossroads[port]

        if i % 2 == 0:
            port2 = ports[i + 1]
            cross2 = crossroads[port2]
            cross.special_port = label[i / 2]
            cross2.special_port = label[i / 2]
            pygame.draw.line(board_window, (124, 124, 124), (int(cross.location_x), int(cross.location_y)),
                             (int(cross2.location_x), int(cross2.location_y)), 10)
            port_label = myfont.render(label[i / 2], 1, (0, 0, 0))
            new_x = (int(cross.location_x) + int(cross2.location_x))/2
            new_y = (int(cross.location_y) + int(cross2.location_y))/2
            if new_x > 570:
                new_x += 10
            else:
                new_x -= 40
            if new_y < 195:
                new_y -= 25
            elif new_y > 420:
                new_y += 10
            if i / 2 == 4:
                new_y += 10
                new_x -= 10
            if i / 2 == 7:
                new_y -= 10
                new_x -= 14

            board_window.blit(port_label, (new_x, new_y))

    for i in xrange(len(crossroads)):
        if crossroads[i].special_port is not None:
            print i, crossroads[i].special_port


def move_robber(hexagons, board_window):
    clicked = False
    hexagon_with = None
    for hexagon in hexagons:
        if hexagon.robber_on:
            hexagon_with = hexagon
            hexagon_with.robber(board_window)
            print "the robber is no longer on the " + hexagon.resource
    while not clicked:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                for hexagon in hexagons:
                    if hexagon.hex_area.collidepoint(event.pos) and (hexagon.color == board_window.get_at(event.pos)
                                                                     or (255, 255, 255) == board_window.get_at(event.pos)
                                                                     or (0, 0, 0) == board_window.get_at(event.pos)):
                        if hexagon == hexagon_with:
                            print "cant replace the robber in same area"
                        else:
                            print "the robber is placed on the " + hexagon.resource
                            hexagon.robber(board_window)
                            clicked = True
                            break


def draw_road_area(road_area_pts):
    road_areas = []
    for area in road_area_pts:
        if area[0].location[0] == area[1].location[0]:
            road_areas.append((pygame.Rect(area[0].location[0]-10, area[0].location[1],
                                           20, abs(area[1].location[1]-area[0].location[1])),
                               area[0], area[1]))
        elif area[0].location[1] < area[1].location[1]:
            road_areas.append((pygame.Rect(area[0].location[0], area[0].location[1],
                                           abs(area[1].location[0]-area[0].location[0]),
                                           abs(area[1].location[1]-area[0].location[1])), area[0],
                               area[1]))
        else:
            road_areas.append((pygame.Rect(area[0].location[0], area[1].location[1],
                                           abs(area[1].location[0] - area[0].location[0]),
                                           abs(area[1].location[1] - area[0].location[1])),
                               area[0], area[1]))

    print len(road_areas)
    return road_areas


def build_a_road(board_window, road_areas, player, starting):
    clicked = False
    cancel_area = pygame.Rect(20, 560, 90, 30)
    while not clicked:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if cancel_area.collidepoint(event.pos):
                    print "canceled"
                    return
                for road_area in road_areas:
                    if road_area.area.collidepoint(event.pos) and not road_area.occupied \
                            and road_area.is_connected(player):
                        pygame.draw.line(board_window, player.color_rgb, road_area.crossroad_1.location,
                                         road_area.crossroad_2.location, 10)
                        road_area.occupied_by_player(player)
                        clicked = True
                        player.bought_road(starting)
                        break
                    elif road_area.area.collidepoint(event.pos):
                        print "cant build road"


def build_sett(board_window, crossroads, crossroad_area, player):
    clicked = False
    print "build a town"
    cancel_area = pygame.Rect(20, 560, 90, 30)
    while not clicked:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if cancel_area.collidepoint(event.pos):
                    print "canceled"
                    return
                for crossroad_area_check in crossroad_area:
                    if crossroad_area_check.collidepoint(event.pos):
                        for crossroad in crossroads:
                            if crossroad_area_check.collidepoint(crossroad.location):
                                clicked = crossroad.build_settlement(board_window, False, player)
                                player.bought_sett()
                                break


def build_sett_first(board_window, crossroads, crossroad_area, player):
    clicked = False
    c = None
    while not clicked:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                for crossroad_area_check in crossroad_area:
                    if crossroad_area_check.collidepoint(event.pos):
                        for crossroad in crossroads:
                            if crossroad_area_check.collidepoint(crossroad.location):
                                clicked = crossroad.build_settlement(board_window, True, player)
                                c = crossroad
                                break
    return c


def build_new_city(board_window, crossroads, crossroad_area, player):
    clicked = False
    print "build a town"
    cancel_area = pygame.Rect(20, 560, 90, 30)
    while not clicked:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if cancel_area.collidepoint(event.pos):
                    print "canceled"
                    return
                for crossroad_area_check in crossroad_area:
                    if crossroad_area_check.collidepoint(event.pos):
                        for crossroad in crossroads:
                            if crossroad_area_check.collidepoint(crossroad.location):
                                clicked = crossroad.build_city(board_window, player)
                                break


def create_crossroads(hexagons):
    crossroads_points = []

    for hexagon in hexagons:
        pts = hexagon.calc_pts()
        for point in pts:
            available = True
            for crossroad in crossroads_points:
                if 2 > ((point[0] - crossroad[0])**2 + (point[1] - crossroad[1])**2):
                    available = False
            if available:
                crossroads_points.append(point)

    crossroads = []
    crossroads_area = []

    for crossroad_point in crossroads_points:
        crossroads.append(Crossroad(crossroad_point))

    print len(crossroads)

    for crossroad in crossroads:
        for roadcross in crossroads:
            crossroad.near_crossroad(roadcross)
        crossroads_area.append(pygame.Rect(crossroad.location_x - 15, crossroad.location_y - 15, 30, 30))

    return crossroads, crossroads_area


def create_roads(crossroads):
    roads_area_temp = []
    roads_area = []

    for crossroad in crossroads:
        for near in crossroad.near_crossroads:
            if near.location_x < crossroad.location_x:
                roads_area_temp.append((near, crossroad))
            elif near.location_x == crossroad.location_x and near.location_y < crossroad.location_y:
                roads_area_temp.append((near, crossroad))
            else:
                roads_area_temp.append((crossroad, near))

    for area1 in roads_area_temp:
        if area1 not in roads_area:
            roads_area.append(area1)
    print len(roads_area)

    return roads_area


def create_roads_list(roads_area):
    roads = []
    for area in roads_area:
        roads.append(Road(area[1], area[2], area[0]))

    return roads


def roll_dice(hexagons, board_window):
    die_1 = random.randint(1, 6)
    die_2 = random.randint(1, 6)
    dice = die_1 + die_2
    print "rolled: ", die_1, die_2
    if dice == 7:
        print "please move the robber"
        move_robber(hexagons, board_window)
    else:
        for hexagon in hexagons:
            if dice == hexagon.dice_number and not hexagon.robber_on:
                hexagon.got_rolled()
            elif dice == hexagon.dice_number:
                print "there is a robber on the " + hexagon.resource


def create_development_cards():
    dev = []
    for i in xrange(14):
        dev.append("KNIGHT")
        if i % 7 == 0:
            dev.append("BUILD 2 ROADS")
            dev.append("MONOPOLY")
            dev.append("YEAR OF PLENTY")
        if i < 5:
            dev.append("VICTORY POINT")
    return dev


def draw_dev_card(deck, player):
    if len(deck) > 0:
        card = deck.pop(0)
        print player.name, "drew the", card, "card"
        player.dev_cards.append(card)
        print player.dev_cards
        player.bought_dev()
    else:
        print "no more dev cards"


def switch_players(win, player):
    move_player(win)
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 18)
    stringer = str(player.name) + " has:\r\n" + str(player.resource_wheat) + " wheat\r\n" + str(player.resource_iron)\
               + " iron\r\n" + str(player.resource_wood) + " wood\r\n" + str(player.resource_brick) + " brick\r\n"\
               + str(player.resource_sheep) + " sheep"
    blit_text(win, stringer, (20, 235), myfont, "white")


def move_player(win):
    pygame.draw.rect(win, (0, 0, 0), (20, 237, 100, 200))


def blit_text(surface, text, pos, font, color):
    color = pygame.Color(color)
    words = [word.split(' ') for word in text.splitlines()]  # 2D array where each row is a list of words.
    space = font.size(' ')[0]  # The width of a space.
    max_width, max_height = surface.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.


def get_first_resources(hexagons, first_cross):
    for hax in hexagons:
        for cross in hax.crossroads_by:
            if cross.occupied_by is not None and cross == first_cross:
                print cross.player_occuping.name, "got ", hax.resource
                cross.player_occuping.add_resource(hax.resource, 1)


def error_box(title, message):
    root = Tk()
    root.title(title)
    label = Label(root)
    label.pack()
    msg = Message(root, text=message)
    msg.config(fg='red', font=('times', 24))
    msg.pack()
    button = Button(root, text='OK', width=25, command=root.destroy)
    button.pack()
    root.mainloop()


def bank_trading(port, player):
    global root1

    root1.destroy()
    root_4 = Tk()

    frametop = Frame(root_4)
    framebottom = Frame(root_4)
    frameleft = Frame(framebottom)
    frameright = Frame(framebottom)

    if port == "4:1":
        text = Message(frameleft, text="you give the bank 4:")
    elif port == "3:1":
        text = Message(frameleft, text="you give the bank 3:")
    else:
        text = Message(frameleft, text="you give the bank 2" + port.split(" ")[1])
    text.config(fg='red', font=('times', 10))
    text.pack()
    text2 = Message(frameright, text="you will get 1:")
    text2.config(fg='red', font=('times', 10))
    text2.pack()

    v = IntVar()
    v2 = IntVar()

    resources = {1: "wood", 2: "wheat", 3: "brick", 4: "sheep", 5: "iron"}

    if "2:1" not in port:
        btn1 = Radiobutton(frameleft, text="wood", padx=20, variable=v, value=1)
        btn2 = Radiobutton(frameleft, text="wheat", padx=20, variable=v, value=2)
        btn3 = Radiobutton(frameleft, text="brick", padx=20, variable=v, value=3)
        btn4 = Radiobutton(frameleft, text="sheep", padx=20, variable=v, value=4)
        btn5 = Radiobutton(frameleft, text="iron", padx=20, variable=v, value=5)
    btn6 = Radiobutton(frameright, text="wood", padx=20, variable=v2, value=1)
    btn7 = Radiobutton(frameright, text="wheat", padx=20, variable=v2, value=2)
    btn8 = Radiobutton(frameright, text="brick", padx=20, variable=v2, value=3)
    btn9 = Radiobutton(frameright, text="sheep", padx=20, variable=v2, value=4)
    btn10 = Radiobutton(frameright, text="iron", padx=20, variable=v2, value=5)

    button1 = Button(framebottom, text='I made my choice', width=25, command=root_4.destroy)

    frametop.pack(side=TOP, fill=BOTH, expand=1)
    framebottom.pack(side=BOTTOM, fill=BOTH, expand=1)
    frameleft.pack(side=LEFT, fill=BOTH, expand=1)
    frameright.pack(side=RIGHT, fill=BOTH, expand=1)

    if "2:1" not in port:
        btn1.pack(fill=BOTH, padx=5, pady=5, expand=1)
        btn2.pack(fill=BOTH, padx=5, pady=5, expand=1)
        btn3.pack(fill=BOTH, padx=5, pady=5, expand=1)
        btn4.pack(fill=BOTH, padx=5, pady=5, expand=1)
        btn5.pack(fill=BOTH, padx=5, pady=5, expand=1)
    btn6.pack(fill=BOTH, padx=5, pady=5, expand=1)
    btn7.pack(fill=BOTH, padx=5, pady=5, expand=1)
    btn8.pack(fill=BOTH, padx=5, pady=5, expand=1)
    btn9.pack(fill=BOTH, padx=5, pady=5, expand=1)
    btn10.pack(fill=BOTH, padx=5, pady=5, expand=1)
    button1.pack(side=BOTTOM)
    root_4.mainloop()
    if port == "3:1":
        trade_with_bank(player, resources[v.get()], resources[v2.get()], 3)
    elif port == "4:1":
        trade_with_bank(player, resources[v.get()], resources[v2.get()], 4)
    else:
        trade_with_bank(player, port.split(" ")[1], resources[v2.get()], 2)


def trade_with_bank(player, giving, getting, amount):
    if player.what_resource(giving) > amount - 1:
        print amount, getting, giving
        player.trade(amount, giving, getting)
        print "ade"


def trade(player, player_list):
    global root
    root = Tk()
    root.title("trading")
    label = Label(root)
    label.pack()
    msg = Message(root, text="who would you like to trade with?")
    msg.config(fg='red', font=('times', 24))
    msg.pack()
    button = Button(root, text='player', width=25, command=functools.partial(player_trade, player, player_list))
    button2 = Button(root, text='bank', width=25, command=functools.partial(bank_trade, player))
    button.pack(side=LEFT)
    button2.pack(side=RIGHT)
    root.mainloop()


def player_trade(player, player_list):
    global root, root1
    root.destroy()
    root1 = Tk()
    root1.title("player trading")
    label1 = Label(root1)
    label1.pack()
    msggg = Message(root1, text="with whom would you like to trade with?")
    msggg.config(fg='red', font=('times', 24))
    msggg.pack()
    for p in player_list:
        if p != player:
            player_button = Button(root1, text=p.name, width=25, command=functools.partial(hi, player, p))
            player_button.pack()
    root1.mainloop()


def bank_trade(player):
    global root, root1
    root.destroy()
    ports = player.bank_trades
    root1 = Tk()
    root1.title("bank trading")
    label1 = Label(root1)
    label1.pack()
    msggg = Message(root1, text="with whet port would you like to trade with?")
    msggg.config(fg='red', font=('times', 24))
    msggg.pack()
    for port in ports:
        bank = Button(root1, text=port, width=25, command=functools.partial(bank_trading, port, player))
        bank.pack(side=LEFT)
    root1.mainloop()


def main():
    c_list = COLOR_LIST
    player_list = []
    for i in xrange(3):
        name = raw_input("input name -> ")
        #color = raw_input("chose a color " + str(c_list) + " -> ")
        color = c_list[0]
        while color not in c_list:
            print c_list
            color = raw_input("chose a color " + str(c_list) + " -> ")
        c_list.remove(color)
        p = Player(color, name)
        player_list.append(p)
    print len(player_list)
    player = player_list[0]
    player_counter = 0
    board_window = pygame.display.set_mode((900, 600))
    pygame.display.set_caption("catan board")
    board_window.fill((49, 100, 255))

    hexagons, roads, crossroads, crossroads_area = draw_board(board_window)

    create_ports(crossroads, board_window)

    run = True

    roll_area = pygame.Rect(20, 20, 90, 30)
    build_road_area = pygame.Rect(20, 55, 90, 30)
    build_town_area = pygame.Rect(20, 90, 90, 30)
    next_player_area = pygame.Rect(20, 125, 90, 30)
    dev_card_area = pygame.Rect(20, 160, 90, 30)
    build_city_area = pygame.Rect(20, 195, 90, 30)
    trade_area = pygame.Rect(115, 55, 90, 30)

    development_cards_deck = create_development_cards()
    shuffle(development_cards_deck)
    print development_cards_deck

    pygame.display.update()

    for hexagon in hexagons:
        for cross in crossroads:
            for road in hexagon.crossroads:
                if 2 > math.sqrt((cross.location[0] - road[0])**2 + (cross.location[1] - road[1])**2):
                    hexagon.add_crossroad(cross)

    for i in xrange(3):
        player = player_list[i]
        switch_players(board_window, player)
        pygame.display.update()
        build_sett_first(board_window, crossroads, crossroads_area, player)
        pygame.display.update()
        build_a_road(board_window, roads, player, True)
        pygame.display.update()

    for i in xrange(3):
        player = player_list[2-i]
        switch_players(board_window, player)
        pygame.display.update()
        first_cross = build_sett_first(board_window, crossroads, crossroads_area, player)
        get_first_resources(hexagons, first_cross)
        switch_players(board_window, player)
        pygame.display.update()
        build_a_road(board_window, roads, player, True)
        pygame.display.update()

    switch_players(board_window, player)

    rolled = False

    while run:
        if player.victory_points > 2:
            run = False
            player.winner()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1 and roll_area.collidepoint(event.pos) :
                    roll_dice(hexagons, board_window)
                    switch_players(board_window, player)
                    rolled = True
                elif event.button == 1 and build_road_area.collidepoint(event.pos) and rolled:
                    if player.resource_brick >= 1 and player.resource_wood >= 1:
                        build_a_road(board_window, roads, player, False)
                        switch_players(board_window, player)
                    else:
                        error_box("can't build road", "not enough resources, you need at least:\n1 wood\nand 1 brick")
                elif event.button == 1 and build_town_area.collidepoint(event.pos) and rolled:
                    if player.resource_brick >= 1 and player.resource_wood >= 1 \
                            and player.resource_sheep >= 1 and player.resource_wheat >= 1:
                        build_sett(board_window, crossroads, crossroads_area, player)
                        switch_players(board_window, player)
                    else:
                        error_box("can't build new settlement",
                                  "not enough resources,you need at least: \n1 wood \n1 brick \n1 sheep \nand 1 wheat")
                elif event.button == 1 and next_player_area.collidepoint(event.pos) and rolled:
                    player_counter += 1
                    if player_counter == 3:
                        player_counter = 0
                    player = player_list[player_counter]
                    switch_players(board_window, player)
                    rolled = False
                    print player.name, "is now playing"
                elif event.button == 1 and dev_card_area.collidepoint(event.pos) and rolled:
                    if player.resource_wheat >= 1 and player.resource_sheep >= 1 and player.resource_iron >= 1:
                        draw_dev_card(development_cards_deck, player)
                        switch_players(board_window, player)
                    else:
                        error_box("can't buy new development card",
                                  "not enough resources,you need at least: \n1 iron \n1 sheep \nand 1 wheat")
                elif event.button == 1 and build_city_area.collidepoint(event.pos) and rolled:
                    if player.resource_wheat >= 2 and player.resource_iron >= 3:
                        build_new_city(board_window, crossroads, crossroads_area, player)
                    else:
                        error_box("can't build city", "not enough resources,you need at least: \n3 iron\nand 2 wheat")
                elif event.button == 1 and trade_area.collidepoint(event.pos):
                    trade(player, player_list)
                    switch_players(board_window, player)
                else:
                    if event.button == 1:
                        for hexagon in hexagons:
                            if hexagon.hex_area.collidepoint(event.pos) \
                                    and (hexagon.color == board_window.get_at(event.pos)
                                         or (255, 255, 255) == board_window.get_at(event.pos)
                                         or (0, 0, 0) == board_window.get_at(event.pos)):
                                print hexagon.resource
                                break
        pygame.display.update()

if __name__ == '__main__':
    main()