__author__ = 'Cyber-01'

import pygame
import math
from random import shuffle
import pyaudio


class Hex:

    def __init__(self, location, resource, dice_num, color):
        self.location = location
        self.resource = resource
        self.dice_number = dice_num
        self.crossroads = []
        self.color = color
        x, y = self.location
        for i in range(6):
            x = x + 65 * math.cos(math.pi/6 + math.pi * 2 * i / 6)
            y = y + 65 * math.sin(math.pi/6 + math.pi * 2 * i / 6)
            self.crossroads.append([int(x), int(y)])

    def print_crossroad(self):
        print self.crossroads

    def draw_hex(self, surface):
        self.pts = []
        x, y = self.location
        for i in range(6):
            x = x + 65 * math.cos(math.pi/6 + math.pi * 2 * i / 6)
            y = y + 65 * math.sin(math.pi/6 + math.pi * 2 * i / 6)
            self.pts.append([x, y])
        pygame.draw.polygon(surface, self.color, self.pts)

        for i in range(6):
            if i == 5:
                pygame.draw.line(surface, (0, 0, 0), self.pts[i], self.pts[0])
            else:
                pygame.draw.line(surface, (0, 0, 0), self.pts[i], self.pts[i+1])


        if self.resource != "desert":
            pygame.font.init()
            myfont = pygame.font.SysFont('Comic Sans MS', 26)
            center_x, center_y = self.location
            center_y -= 2*(65 * math.sin(math.pi/6 + math.pi * 2 * 3 / 6))

            pygame.draw.circle(surface, (255, 255, 255), (int(center_x), int(center_y)), 22)
            label = myfont.render(str(self.dice_number), 1, (0, 0, 0))
            surface.blit(label, (int(center_x) - (14 if self.dice_number > 9 else 8), int(center_y) - 17))

    def get_next_y(self):
        return int(self.crossroads[3][1])


def draw_board(board_window):
    wheat_hex_color = (229, 219, 79)
    wood_hex_color = (75, 132, 90)
    sheep_hex_color = (11, 255, 77)
    iron_hex_color = (165, 168, 169)
    brick_hex_color = (104, 42, 24)
    desert_hex_color = (0, 0, 0)

    hex_lst = [("wheat", wheat_hex_color), ("wheat", wheat_hex_color), ("wheat", wheat_hex_color), ("wheat", wheat_hex_color), ("wood", wood_hex_color), ("wood", wood_hex_color), ("wood", wood_hex_color), ("wood", wood_hex_color), ("sheep", sheep_hex_color), ("sheep", sheep_hex_color), ("sheep", sheep_hex_color), ("sheep", sheep_hex_color), ("iron", iron_hex_color), ("iron", iron_hex_color), ("iron", iron_hex_color), ("brick", brick_hex_color), ("brick", brick_hex_color), ("brick", brick_hex_color), ("desert", desert_hex_color)]
    nums = [2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12]


    shuffle(hex_lst)
    shuffle(nums)


    hexagons = []

    num_place = 0
    y = 45
    x = 233
    hex_jump = 2*(65 * math.cos(math.pi/6 + math.pi * 2 * 5 / 6))
    row_x_1 = x - (hex_jump/2)
    row_x_2 = x - hex_jump

    for i in range(19):
        if hex_lst[i][0] != "desert":
            hexagons.append(Hex((x, y), hex_lst[i][0], nums[num_place], hex_lst[i][1]))
            num_place += 1
        else:
            hexagons.append(Hex((x, y), hex_lst[i][0], 0, hex_lst[i][1]))
        x += hex_jump
        if i == 2 or i == 6 or i == 11 or i == 15:
            if i == 2 or i == 11:
                x = row_x_1
            elif i == 6:
                x = row_x_2
            else:
                x = 233
            y = hexagons[i].get_next_y()

    for hexagon in hexagons:
        hexagon.draw_hex(board_window)


def main():
    board_window = pygame.display.set_mode((700, 600))
    pygame.display.set_caption("catan board")
    board_window.fill((49, 100, 255))

    draw_board(board_window)

    run = True

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        pygame.display.update()

if __name__ == '__main__':
    main()

